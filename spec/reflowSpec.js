var Reflow = require("../reflow.js");

describe("Reflow", function(){
	it("has a default `table_width`", function(){
		var r = new Reflow();
		expect(r.table_width).toBe(4);
	});

	it("has a default `table_height`", function(){
		var r = new Reflow();
		expect(r.table_height).toBe(3);
	});

	it("defines the block types", function(){
		var r = new Reflow();
		expect(r.BLOCK_TYPES.BIG).toEqual({ width: 2, height: 2 });
		expect(r.BLOCK_TYPES.ROW_HIGH).toEqual({ width: 1, height: 2 });
		expect(r.BLOCK_TYPES.ROW_LONG).toEqual({ width: 2, height: 1 });
		expect(r.BLOCK_TYPES.SQUARE).toEqual({ width: 1, height: 1 });
	});
});

describe("Reflow.reflow#check", function(){
	it("checks that given blocks cannot be more that `width * height`", function(){
		var r = new Reflow();

		expect(() => r.reflow([
			r.BLOCK_TYPES.BIG,
			r.BLOCK_TYPES.BIG,
			r.BLOCK_TYPES.BIG,
			r.BLOCK_TYPES.BIG
		])).toThrow("blocks cannot be stored!");

		expect(() => r.reflow([
			r.BLOCK_TYPES.ROW_HIGH,
			r.BLOCK_TYPES.ROW_HIGH,
			r.BLOCK_TYPES.ROW_HIGH,
			r.BLOCK_TYPES.ROW_HIGH,
			r.BLOCK_TYPES.ROW_LONG,
			r.BLOCK_TYPES.ROW_LONG
		])).not.toThrow();
	});
});

describe("Reflow.reflow#solutions", function(){
	it("returns solution as array", function(){
		var r = new Reflow();

		expect(r.reflow([
			r.BLOCK_TYPES.BIG,
			r.BLOCK_TYPES.BIG,
			r.BLOCK_TYPES.ROW_LONG,
			r.BLOCK_TYPES.ROW_LONG
		]).length).toBe(3);
	});

	it("returns indexes of the blocks", function(){
		var r = new Reflow();

		(function(){
			var blocks = [
				r.BLOCK_TYPES.SQUARE,r.BLOCK_TYPES.SQUARE, r.BLOCK_TYPES.SQUARE, r.BLOCK_TYPES.SQUARE,
				r.BLOCK_TYPES.SQUARE,r.BLOCK_TYPES.SQUARE, r.BLOCK_TYPES.SQUARE, r.BLOCK_TYPES.SQUARE,
				r.BLOCK_TYPES.SQUARE,r.BLOCK_TYPES.SQUARE, r.BLOCK_TYPES.SQUARE, r.BLOCK_TYPES.SQUARE
			];

			var result = r.reflow(blocks);
			expect(result).toEqual([
				[0, 1, 2, 3],
 			 	[4, 5, 6, 7],
				[8, 9, 10, 11]
			]);
		})();

		(function(){
			var blocks = [
				r.BLOCK_TYPES.BIG, r.BLOCK_TYPES.BIG, r.BLOCK_TYPES.ROW_LONG, r.BLOCK_TYPES.ROW_LONG
			];
			var result = r.reflow(blocks);
			expect(result).toEqual([
				[0, 1],
				[],
				[2, 3]
			]);
		})();
	});

	it("does not (leave empty blocks | cover two blocks)", function(){
		var r = new Reflow();

		r.addSquareBlock()
			.addRowHighBlock()
			.addRowLongBlock()
			.addBigBlock()
			.addRowHighBlock()
			.addSquareBlock();

		var result = r.reflow();
		expect(result).toEqual([
			[0, 1, 2],
			[4, 3],
			[5]
		]);
	});
});

describe("Reflow#addXXXBlock", function(){
	it("make reflow work as if an explicit list had been passed as parameter", function(){
		var r = new Reflow();

		r.addBigBlock()
			.addBigBlock()
			.addRowLongBlock()
			.addRowLongBlock();
		var result = r.reflow();

		expect(result).toEqual([
			[0, 1],
			[],
			[2, 3]
		]);

		// remove last reflow blocks!
		r.cleanAddedBlocks();
		for(var i=0; i<12; i++) r.addSquareBlock();

		var result = r.reflow();
			expect(result).toEqual([
				[0, 1, 2, 3],
				[4, 5, 6, 7],
				[8, 9, 10, 11]
			]);
	})
})
