var Reflow = function(){
	this.table_width = 4;
	this.table_height = 3;

	this.BLOCK_TYPES = {
		BIG: { width: 2, height: 2},
		ROW_HIGH: { width: 1, height: 2 },
		ROW_LONG: { width: 2, height: 1 },
		SQUARE: { width: 1, height: 1 }
	};

	this._FREE_BLOCK = -1;
	this._internalTable = this._buildCleanTable();

	this._addedList = [];
};

Reflow.prototype.reflow = function(blocksList){
	if(blocksList === undefined){
		return this.reflow(this._addedList);
	}

	if(!this._checkAgainstCapacity(blocksList)){
		throw "blocks cannot be stored!"
	}

  // perform cleanup between each reflow
  this._internalTable = this._buildCleanTable();

	// for now, let's assume that the block list is valid
	var result = this._buildEmptySolution();
	blocksList.forEach(function(block, block_index){
		var block_position = this._positionForBlock(block);
		var row_index = block_position.x;
		if(row_index === -1){
			throw "cannot place block #"+block_index;
		}
		this._saveBlockPosition(block_position, block, block_index);
	}.bind(this));

  // calculate result
  result = this._resultFromInternalTable(blocksList.length);

	return result;
}

Reflow.prototype._printInternalTable = function(block){
	console.log("executed block", block);
	this._internalTable.forEach(function(row, rowIdx){
		console.log("[table#"+rowIdx+"]", row);
	});
}

Reflow.prototype._buildCleanTable = function(){
	var cleanTable = [];
	for(var hh=0; hh<this.table_height; hh++){
		cleanTable.push([]);
		for(var ww=0; ww<this.table_width; ww++){
			cleanTable[hh].push(this._FREE_BLOCK);
		}
	}
	return cleanTable
}

Reflow.prototype._checkAgainstCapacity = function(blocksList){
	return blocksList.reduce(function(acc, it){
		return acc + it.width * it.height
	}, 0) === this.table_width * this.table_height;
}

Reflow.prototype._buildEmptySolution = function(){
	var result = [];
	for(var i=0; i<this.table_height; i++){
		result.push([]);
	}
	return result;
}

Reflow.prototype._positionForBlock = function(block){
	var rowForBlock = -1;
	var columnForBlock = -1;
	var _FREE_CONSTANT = this._FREE_BLOCK;
	this._internalTable.some(function(_row, _rowIdx){
		var freeIndexes = [];
		_row.forEach(function(item, idx){
			if(item === _FREE_CONSTANT){
				freeIndexes.push(idx);
			}
		});

    freeIndexes.some(function(startColumn){
			// check there is enough continuous space to place the block
			var isFree = true;
			for(var hh=0; hh<block.height; hh++){
				for(var ww=0; ww<block.width; ww++){
					// bound checking
					if(_rowIdx+hh >= this.table_height || startColumn+ww >= this.table_width){
						isFree = false
					}
					else{
						isFree = isFree && (this._internalTable[_rowIdx+hh][startColumn+ww] === -1);
					}
				}
			}
			if(isFree){
				rowForBlock = _rowIdx
				columnForBlock = startColumn
				return true;
			}
		}.bind(this));
		
		if(rowForBlock !== -1){
			return true;
		}

	}.bind(this));
	return {
		x: rowForBlock,
		y: columnForBlock
	};
}

Reflow.prototype._saveBlockPosition = function(block_position, block, block_index){
	var row_index = block_position.x;
	var startColumn = block_position.y;
	for(var hh=0; hh<block.height; hh++){
		for(var ww=0; ww<block.width; ww++){
			this._internalTable[row_index+hh][startColumn+ww] = block_index;
		}
	}
}

Reflow.prototype._resultFromInternalTable = function(blocks_number){
	var result = [];

	var availableBlocks  = new Set();
	for(var i=0; i<blocks_number; i++){
		availableBlocks.add(i);
	}

	this._internalTable.forEach(function(row, rowIdx){
		result.push([]);
		row.forEach(function(blockId){
			if(availableBlocks.delete(blockId)){
				result[rowIdx].push(blockId);
			}
		});
	})

	return result;
}

// syntactic sugar

Reflow.prototype.addBigBlock = function(){
	this._addedList.push(this.BLOCK_TYPES.BIG);
	return this;
}

Reflow.prototype.addRowLongBlock = function(){
	this._addedList.push(this.BLOCK_TYPES.ROW_LONG);
	return this;
}

Reflow.prototype.addRowHighBlock = function(){
	this._addedList.push(this.BLOCK_TYPES.ROW_HIGH);
	return this;
}

Reflow.prototype.addSquareBlock = function(){
	this._addedList.push(this.BLOCK_TYPES.SQUARE);
	return this;
}

Reflow.prototype.cleanAddedBlocks = function(){
	this._addedList = [];
}

if(typeof module !== "undefined"){
	module.exports = Reflow
}
