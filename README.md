Reflow
======

This little module takes a list of blocks and rearranges them
to stay in a N x M rectangle.

Blocks can be:

* 1x1
* 1x2
* 2x1
* 2x2
